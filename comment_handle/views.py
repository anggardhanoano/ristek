from django.shortcuts import render,redirect
from .models import CommentModel
from .forms import CommentForm
# handle the post method off comment using def
def CommentView(request) :
    # simply just take the value from the form
    # check is the form valid, if yes then save the value to the model
    if request.method == 'POST' :
        form = CommentForm(request.POST)
        if form.is_valid() :
            instance = form.save()
            return redirect("home:home")
    
    return redirect("home:home")
    

def HomePageView(request) :
    # retriev all the comment to the homepage 
    comment = CommentModel.objects.all().order_by('date')
    return render(request, 'homepage.html', {
        'comment' : comment,
        'form' : CommentForm,
    })
        
            


