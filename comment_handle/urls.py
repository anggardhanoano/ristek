from django.conf.urls import include
from django.urls import path
from .views import HomePageView, CommentView


app_name = "home"

urlpatterns = [
    path('',HomePageView,name = "home"),
    path('comment', CommentView, name = "comment"),
]
