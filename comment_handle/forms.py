from django import forms
from comment_handle.models import CommentModel

class CommentForm(forms.ModelForm) :

    author = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "author",
                'class' : 'form-control',
                }
    ))

    comment = forms.CharField(label="", widget=forms.Textarea(
    attrs = {
            "id" : "author-text",
            'class' : 'form-control',
            }
    ))

    class Meta :
        model = CommentModel
        fields = ("author", "comment")