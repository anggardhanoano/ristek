from django.db import models

# Create your models here.

class CommentModel(models.Model) :
    author = models.EmailField(max_length=120)
    comment = models.TextField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.author} comment'

