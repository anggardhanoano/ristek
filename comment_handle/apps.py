from django.apps import AppConfig


class CommentHandleConfig(AppConfig):
    name = 'comment_handle'
